<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model {
    protected $guarded = [];

    public static function rules($scenario) {
        switch ($scenario) {
            case 'create':
                return [
                    'first_name' => 'required|max:80',
                    'last_name' => 'required|max:80',
                    'age' => 'required|numeric|max:130',
                    'phone' => 'required|max:20',
                    'email' => 'required|email',
                    'password' => 'required|max:80',
                    'hourly_rate' => 'required|numeric',
                ];
            case 'update':
                return [
                    'first_name' => 'max:80',
                    'last_name' => 'max:80',
                    'age' => 'numeric|max:130',
                    'phone' => 'max:20',
                    'email' => 'email',
                    'password' => 'max:80',
                    'hourly_rate' => 'numeric',
                ];
        }
    }
}
