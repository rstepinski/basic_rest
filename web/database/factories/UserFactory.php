<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$factory->define(User::class, function (Faker $faker) {
    $gender = Arr::random(['female', 'male']);

    return [
        'first_name' => $faker->firstName($gender),
        'last_name' => $faker->lastName($gender),
        'age' => $faker->numberBetween(18, 70),
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'password' => $faker->password,
        'hourly_rate' => $faker->randomFloat(2, 17, 100),
    ];
});
