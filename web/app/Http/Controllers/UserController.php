<?php


namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller {

    /**
     * Retrieve the list of all users.
     */
    public function list() {
        return User::all();
    }

    /**
     * Retrieve a single user by id.
     */
    public function show($id) {
        return User::findOrFail($id);
    }

    /**
     * Create a new user.
     */
    public function create(Request $request) {
        $this->validate($request, User::rules('create'));
        if (User::create($request->all())) {
            return new JsonResponse([
                'message' => 'User created',
            ]);
        }
        else throw new \Exception("User couldn't be created");
    }

    /**
     * Update existing user.
     */
    public function update($id, Request $request) {
        $user = User::findOrFail($id);
        $this->validate($request, User::rules('update'));
        $user->fill($request->all());
        if ($user->save()) {
            return new JsonResponse([
                'message' => 'User updated',
            ]);
        }
        else throw new \Exception("User couldn't be updated");
    }

    /**
     * Delete the specified user.
     */
    public function delete($id) {
        $user = User::findOrFail($id);
        if ($user->delete()) {
            return new JsonResponse([
                'message' => 'User deleted',
            ]);
        }
        else throw new \Exception("User couldn't be deleted");
    }
}
