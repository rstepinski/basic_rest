# Basic REST

This project contains an example REST API for managing a list of people/users.

## Local setup

Clone this repository, then navigate to its location.
Run the following commands:

```
$ cd web
$ composer install
$ cp .env.example .env
```

If you need to make any adjustments to the `.env` file, now is the time to do so.

If you're using **Traefik**, set the correct network name in `docker-compose.dev.yml`, 
unless yours is called `traefik`, in which case you don't need to
change anything. Rename the `docker-compose.dev.yml` file to `docker-compose.yml`.

If you're not using Traefik, rename `docker-compose.no-traefik.yml` to `docker-compose.yml` instead.

Go back to root directory of the project and run the
following:

```
$ docker-compose up -d
$ docker-compose exec -w /app web php artisan migrate
$ docker-compose exec -w /app web php artisan db:seed
```

### What is Docker

If you don't want to use Docker, you'll need to provide a MySQL server
and provide information about it in the `.env` file.

Then run the following unless you already have a PHP server:

```
$ cd web
$ php -S localhost:8000 -t public
```

Now open a new terminal window and run the following:

```
$ cd web
$ php artisan migrate
$ php artisan db:seed
```

## Available endpoints

`GET /users` - lists all users

`GET /users/:id` - shows one user by ID

`POST /users` - creates a user, requires all following parameters: `first_name`, `last_name`, `age`, `phone`, `email`, `password`, `hourly_rate`.

`PUT /users/:id` - updates a user by ID, accepts any parameters used by previous endpoint.

`DELETE /users/:id` - deletes a user. 