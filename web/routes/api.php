<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\JsonResponse;

$router->get('/', function () {
   return new JsonResponse([
      'available_endpoints' => [
          'GET /users',
          'POST /users',
          'GET /users/:id',
          'PUT /users/:id',
          'DELETE /users/:id',
      ] ,
   ]);
});

$router->get('/users', 'UserController@list');
$router->post('/users', 'UserController@create');

$router->get('/users/{id}', 'UserController@show');
$router->put('/users/{id}', 'UserController@update');
$router->delete('/users/{id}', 'UserController@delete');
